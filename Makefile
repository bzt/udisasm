all: disasm

aarch64.h: aarch64.txt
	php ./txt2h.php aarch64.txt

x86_64.h: x86_64.txt
	php ./txt2h.php x86_64.txt

disasm: aarch64.h main.c
	gcc -Wall -Wextra -Wno-format-extra-args main.c -o disasm

clean:
	rm disasm || true
