/* Universal Disassembler Function Library
 * https://gitlab.com/bztsrc/udisasm
 *
 *            ----- GENERATED FILE, DO NOT EDIT! -----
 *
 * Copyright (C) 2017 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Disassembler source generated from java.txt
 */

#ifdef __cplusplus
extern "C" {
#endif

#define disasm_arch "java"
enum { disasm_arg_NONE,disasm_arg_ofs,disasm_arg_ofe, disasm_arg_a0, disasm_arg_label, disasm_arg_label4, disasm_arg_a1, disasm_arg_a2 };

/*** private functions ***/
char *disasm_str(char*s,int n) {if(!s)return "?";while(n){s++;if(!*s){s++;n--;}}return *s?s:"?";}

/*** public API ***/
uint64_t disasm(uint64_t addr, char *str)
{
    uint16_t op=0, om=0;
    int64_t a0, a1, a2;
    uint8_t ic8;
    uint16_t ic16;
    uint32_t ic32;
    char *names=NULL,*olds=str;
    uint8_t args[4]={0,0,0,0};

    ic8=*((uint8_t*)addr);
    ic16=*((uint16_t*)addr);
    ic32=*((uint32_t*)addr);

    /* handle multiple NOPs at once */
    if(ic8==0x0) {
        while(*((uint8_t*)addr)==ic8) { op++; addr+=1; }
        if(str!=NULL) str+=sprintf(str,"  %d x nop",op);
        *str=0;
        return addr;
    }

    /* decode instruction */
    if(ic32==0xab) {
        names="lookupswitch\0";
        args[0]=disasm_arg_a0; args[1]=disasm_arg_a1; 
        addr+=4;
        a0=(int64_t)(*((int32_t*)addr));addr+=4;
        a1=(int64_t)(*((int32_t*)addr));addr+=4;
        addr+=a1*8;
    } else
    if(ic32==0xc4) {
        names="tableswitch\0";
        args[0]=disasm_arg_a0; args[1]=disasm_arg_a1; args[2]=disasm_arg_a2; 
        addr+=4;
        a0=(int64_t)(*((int32_t*)addr));addr+=4;
        a1=(int64_t)(*((int32_t*)addr));addr+=4;
        a2=(int64_t)(*((int32_t*)addr));addr+=4;
        addr+=(a2-a1)*2;
    } else
    if((ic8&0xfd)==0x11) {
        names="sipush\0ldc_w\0";
        op=((ic8>>1)&0x1); 
        args[0]=disasm_arg_a0; 
        addr+=1;
        a0=(int64_t)(*((int16_t*)addr));addr+=2;
    } else
    if(ic8==0x14) {
        names="ldc2_w\0";
        args[0]=disasm_arg_a0; 
        addr+=1;
        a0=(int64_t)(*((int16_t*)addr));addr+=2;
    } else
    if((ic8&0xd0)==0x10) {
        names="bipush\0sipush\0ldc\0ldc_w\0ldc2_w\0iload\0lload\0fload\0dload\0aload\0iload_0\0iload_1\0iload_2\0iload_3\0lload_0\0lload_1\0faload\0daload\0aaload\0baload\0caload\0saload\0istore\0lstore\0fstore\0dstore\0astore\0istore_0\0istore_1\0istore_2\0istore_3\0lstore_0\0";
        op=((ic8>>1)&0x10)|((ic8)&0xf); 
        args[0]=disasm_arg_a0; 
        addr+=1;
        a0=(int64_t)(*((int8_t*)addr));addr+=1;
    } else
    if((ic16&0xd0ff)==0x10c4) {
        names="?\0?\0?\0?\0?\0iload\0lload\0fload\0dload\0aload\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0istore\0lstore\0fstore\0dstore\0astore\0";
        op=((ic16>>9)&0x10)|((ic16>>8)&0xf); 
        args[0]=disasm_arg_a0; 
        addr+=2;
        a0=(int64_t)(*((int16_t*)addr));addr+=2;
    } else
    if(ic8==0x84) {
        names="iinc\0";
        args[0]=disasm_arg_a0; args[1]=disasm_arg_a1; 
        addr+=1;
        a0=(int64_t)(*((int8_t*)addr));addr+=1;
        a1=(int64_t)(*((int8_t*)addr));addr+=1;
    } else
    if(ic16==0x84c4) {
        names="iinc\0";
        args[0]=disasm_arg_a0; args[1]=disasm_arg_a1; 
        addr+=2;
        a0=(int64_t)(*((int16_t*)addr));addr+=2;
        a1=(int64_t)(*((int16_t*)addr));addr+=2;
    } else
    if((ic8&0xf8)==0x98) {
        names="dcmpg\0ifeq\0ifne\0iflt\0ifge\0ifgt\0ifle\0if_icmpeq\0";
        op=((ic8)&0x7); 
        args[0]=disasm_arg_label; 
        addr+=1;
        a0=(int64_t)(*((int16_t*)addr));addr+=2;
    } else
    if(ic8==0xa9) {
        names="ret\0";
        args[0]=disasm_arg_a0; 
        addr+=1;
        a0=(int64_t)(*((int8_t*)addr));addr+=1;
    } else
    if(ic16==0xa9c4) {
        names="ret\0";
        args[0]=disasm_arg_a0; 
        addr+=2;
        a0=(int64_t)(*((int16_t*)addr));addr+=2;
    } else
    if((ic8&0xf0)==0xa0) {
        names="if_icmpne\0if_icmplt\0if_icmpge\0if_icmpgt\0if_icmple\0if_acmpeq\0if_acmpne\0goto\0jsr\0ret\0?\0?\0ireturn\0lreturn\0freturn\0dreturn\0";
        op=((ic8)&0xf); 
        args[0]=disasm_arg_label; 
        addr+=1;
        a0=(int64_t)(*((int16_t*)addr));addr+=2;
    } else
    if(ic8==0xb9) {
        names="invokeinterface\0";
        args[0]=disasm_arg_a0; args[1]=disasm_arg_a1; 
        addr+=1;
        a0=(int64_t)(*((int16_t*)addr));addr+=2;
        a1=(int64_t)(*((int16_t*)addr));addr+=2;
    } else
    if(ic8==0xba) {
        names="invokedynamic\0";
        args[0]=disasm_arg_a0; 
        addr+=1;
        a0=(int64_t)(*((int32_t*)addr));addr+=4;
    } else
    if(ic8==0xbc) {
        names="newarray\0";
        args[0]=disasm_arg_a0; 
        addr+=1;
        a0=(int64_t)(*((int8_t*)addr));addr+=1;
    } else
    if(ic16==0xbcc4) {
        names="newarray\0";
        args[0]=disasm_arg_a0; 
        addr+=2;
        a0=(int64_t)(*((int16_t*)addr));addr+=2;
    } else
    if(ic16==0xbdc4) {
        names="anewarray\0";
        args[0]=disasm_arg_a0; 
        addr+=2;
        a0=(int64_t)(*((int32_t*)addr));addr+=4;
    } else
    if((ic8&0xf0)==0xb0) {
        names="areturn\0return\0getstatic\0putstatic\0getfield\0putfield\0invokevirtual\0invokespecial\0invokestatic\0invokeinterface\0invokedynamic\0new\0newarray\0anewarray\0arraylength\0athrow\0";
        op=((ic8)&0xf); 
        args[0]=disasm_arg_a0; 
        addr+=1;
        a0=(int64_t)(*((int16_t*)addr));addr+=2;
    } else
    if((ic8&0xfe)==0xc0) {
        names="checkcast\0instanceof\0";
        op=((ic8)&0x1); 
        args[0]=disasm_arg_a0; 
        addr+=1;
        a0=(int64_t)(*((int16_t*)addr));addr+=2;
    } else
    if(ic8==0xc5) {
        names="multianewarray\0";
        args[0]=disasm_arg_a0; args[1]=disasm_arg_a1; 
        addr+=1;
        a0=(int64_t)(*((int16_t*)addr));addr+=2;
        a1=(int64_t)(*((int8_t*)addr));addr+=1;
    } else
    if(ic16==0xc5c4) {
        names="multianewarray\0";
        args[0]=disasm_arg_a0; args[1]=disasm_arg_a1; 
        addr+=2;
        a0=(int64_t)(*((int32_t*)addr));addr+=4;
        a1=(int64_t)(*((int16_t*)addr));addr+=2;
    } else
    if((ic8&0xfe)==0xc6) {
        names="ifnull\0ifnonnull\0";
        op=((ic8)&0x1); 
        args[0]=disasm_arg_label; 
        addr+=1;
        a0=(int64_t)(*((int16_t*)addr));addr+=2;
    } else
    if((ic8&0xfe)==0xc8) {
        names="goto_w\0jsr_w\0";
        op=((ic8)&0x1); 
        args[0]=disasm_arg_label4; 
        addr+=1;
        a0=(int64_t)(*((int32_t*)addr));addr+=4;
    } else
    {
        names="nop\0aconst_null\0iconst_m1\0iconst_0\0iconst_1\0iconst_2\0iconst_3\0iconst_4\0iconst_5\0lconst_0\0lconst_1\0fconst_0\0fconst_1\0fconst_2\0dconst_0\0dconst_1\0bipush\0sipush\0ldc\0ldc_w\0ldc2_w\0iload\0lload\0fload\0dload\0aload\0iload_0\0iload_1\0iload_2\0iload_3\0lload_0\0lload_1\0lload_2\0lload_3\0fload_0\0fload_1\0fload_2\0fload_3\0dload_0\0dload_1\0dload_2\0dload_3\0aload_0\0aload_1\0aload_2\0aload_3\0iaload\0laload\0faload\0daload\0aaload\0baload\0caload\0saload\0istore\0lstore\0fstore\0dstore\0astore\0istore_0\0istore_1\0istore_2\0istore_3\0lstore_0\0lstore_1\0lstore_2\0lstore_3\0fstore_0\0fstore_1\0fstore_2\0fstore_3\0dstore_0\0dstore_1\0dstore_2\0dstore_3\0astore_0\0astore_1\0astore_2\0astore_3\0iastore\0lastore\0fastore\0dastore\0aastore\0bastore\0castore\0sastore\0pop\0pop2\0dup\0dup_x1\0dup_x2\0dup2\0dup2_x1\0dup2_x2\0swap\0iadd\0ladd\0fadd\0dadd\0isub\0lsub\0fsub\0dsub\0imul\0lmul\0fmul\0dmul\0idiv\0ldiv\0fdiv\0ddiv\0irem\0lrem\0frem\0drem\0ineg\0lneg\0fneg\0dneg\0ishl\0lshl\0ishr\0lshr\0iushr\0lushr\0iand\0land\0ior\0lor\0ixor\0lxor\0iinc\0i2l\0i2f\0i2d\0l2i\0l2f\0l2d\0d2i\0f2l\0f2d\0d2i\0d2l\0d2f\0i2b\0i2c\0i2s\0lcmp\0fcmpl\0fcmpg\0dcmpl\0dcmpg\0ifeq\0ifne\0iflt\0ifge\0ifgt\0ifle\0if_icmpeq\0if_icmpne\0if_icmplt\0if_icmpge\0if_icmpgt\0if_icmple\0if_acmpeq\0if_acmpne\0goto\0jsr\0ret\0?\0?\0ireturn\0lreturn\0freturn\0dreturn\0areturn\0return\0getstatic\0putstatic\0getfield\0putfield\0invokevirtual\0invokespecial\0invokestatic\0invokeinterface\0invokedynamic\0new\0newarray\0anewarray\0arraylength\0athrow\0checkcast\0instanceof\0monitorenter\0monitorexit\0?\0multianewarray\0ifnull\0ifnonnull\0goto_w\0jsr_w\0breakpoint\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0?\0impdep1\0impdep2\0";
        op=((ic8)&0xff); 
        addr+=1;
    }

    if(str!=NULL) {
        str+=sprintf(str,disasm_str(names,op));
        if(str-olds<16)om=16-(str-olds);else om=1;for(op=0;op<om;op++) *str++=' ';
        for(op=0;op<sizeof(args) && args[op]!=disasm_arg_NONE;op++) {
            if(op) { *str++=','; *str++=' '; }
            switch(args[op]) {
                case disasm_arg_a0: str+=sprintf(str,"0x%lx", a0); break;
                case disasm_arg_label: str+=sprintf(str,"0x%lx", (int64_t)((uint16_t)a0)+((int)addr)); break;
                case disasm_arg_label4: str+=sprintf(str,"0x%lx", (int64_t)((uint32_t)a0)+((int)addr)); break;
                case disasm_arg_a1: str+=sprintf(str,"0x%lx", a1); break;
                case disasm_arg_a2: str+=sprintf(str,"0x%lx", a2); break;
                default: break;
            }
            if(*(str-2)==',')str-=2;
        }
        *str=0;
    }
    return addr;
}

#ifdef __cplusplus
}
#endif

