/*
 * Copyright (C) 2017 bzt (bztsrc@gitlab)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Instruction table for Java bytecode
 */

<a0>	"0x%lx", a0
<a1>	"0x%lx", a1
<a2>	"0x%lx", a2
<label>	"0x%lx", (int64_t)((uint16_t)a0)+$
<label4>	"0x%lx", (int64_t)((uint32_t)a0)+$
//turn off sign extension for arguments
@signext

00110010	aaload
01010011	aastore
00000001	aconst_null
00011001+1	aload	<a0>
0001100111000100+2	aload	<a0>
00101010	aload_0
00101011	aload_1
00101100	aload_2
00101101	aload_3
10111101+2	anewarray	<a0>
1011110111000100+4	anewarray	<a0>
10110000	areturn
10111110	arraylength
00111010+1	astore	<a0>
0011101011000100+2	astore	<a0>
01001011	astore_0
01001100	astore_1
01001101	astore_2
01001110	astore_3
10111111	athrow
00110011	baload
01010100	bastore
00010000+1	bipush	<a0>
11001010	breakpoint
00110100	caload
01010101	castore
11000000+2	checkcast	<a0>
10010000	d2f
10001110	d2i
10001111	d2l
01100011	dadd
00110001	daload
01010010	dastore
10011000	dcmpg
10010111	dcmpl
00001110	dconst_0
00001111	dconst_1
01101111	ddiv
00011000+1	dload	<a0>
0001100011000100+2	dload	<a0>
00100110	dload_0
00100111	dload_1
00101000	dload_2
00101001	dload_3
01101011	dmul
01110111	dneg
01110011	drem
10101111	dreturn
00111001+1	dstore	<a0>
0011100111000100+2	dstore	<a0>
01000111	dstore_0
01001000	dstore_1
01001001	dstore_2
01001010	dstore_3
01100111	dsub
01011001	dup
01011010	dup_x1
01011011	dup_x2
01011100	dup2
01011101	dup2_x1
01011110	dup2_x2
10001101	f2d
10001011	d2i
10001100	f2l
01100010	fadd
00110000	faload
01010001	fastore
10010110	fcmpg
10010101	fcmpl
00001011	fconst_0
00001100	fconst_1
00001101	fconst_2
01101110	fdiv
00010111+1	fload	<a0>
0001011111000100+2	fload	<a0>
00100010	fload_0
00100011	fload_1
00100100	fload_2
00100101	fload_3
01101010	fmul
01110110	fneg
01110010	frem
10101110	freturn
00111000+1	fstore	<a0>
0011100011000100+2	fstore	<a0>
01000011	fstore_0
01000100	fstore_1
01000101	fstore_2
01000110	fstore_3
01100110	fsub
10110100+2	getfield	<a0>
10110010+2	getstatic	<a0>
10100111+2	goto	<label>
11001000+4	goto_w	<label4>
10010001	i2b
10010010	i2c
10000111	i2d
10000110	i2f
10000101	i2l
10010011	i2s
01100000	iadd
00101110	iaload
01111110	iand
01001111	iastore
00000010	iconst_m1
00000011	iconst_0
00000100	iconst_1
00000101	iconst_2
00000110	iconst_3
00000111	iconst_4
00001000	iconst_5
01101100	idiv
10100101+2	if_acmpeq	<label>
10100110+2	if_acmpne	<label>
10011111+2	if_icmpeq	<label>
10100010+2	if_icmpge	<label>
10100011+2	if_icmpgt	<label>
10100100+2	if_icmple	<label>
10100001+2	if_icmplt	<label>
10100000+2	if_icmpne	<label>
10011001+2	ifeq	<label>
10011100+2	ifge	<label>
10011101+2	ifgt	<label>
10011110+2	ifle	<label>
10011011+2	iflt	<label>
10011010+2	ifne	<label>
11000111+2	ifnonnull	<label>
11000110+2	ifnull	<label>
10000100+1,1	iinc	<a0>, <a1>
1000010011000100+2,2	iinc	<a0>, <a1>
00010101+1	iload	<a0>
0001010111000100+2	iload	<a0>
00011010	iload_0
00011011	iload_1
00011100	iload_2
00011101	iload_3
11111110	impdep1
11111111	impdep2
01101000	imul
01110100	ineg
11000001+2	instanceof	<a0>
10111010+4	invokedynamic	<a0>
10111001+2,2	invokeinterface	<a0>, <a1>
10110111+2	invokespecial	<a0>
10111000+2	invokestatic	<a0>
10110110+2	invokevirtual	<a0>
10000000	ior
01110000	irem
10101100	ireturn
01111000	ishl
01111010	ishr
00110110+1	istore	<a0>
0011011011000100+2	istore	<a0>
00111011	istore_0
00111100	istore_1
00111101	istore_2
00111110	istore_3
01100100	isub
01111100	iushr
10000010	ixor
10101000+2	jsr	<label>
11001001+4	jsr_w	<label4>
10001010	l2d
10001001	l2f
10001000	l2i
01100001	ladd
00101111	laload
01111111	land
01010000	lastore
10010100	lcmp
00001001	lconst_0
00001010	lconst_1
00010010+1	ldc	<a0>
00010011+2	ldc_w	<a0>
00010100+2	ldc2_w	<a0>
01101101	ldiv
00010110+1	lload	<a0>
0001011011000100+2	lload	<a0>
00011110	lload_0
00011111	lload_1
00100000	lload_2
00100001	lload_3
01101001	lmul
01110101	lneg
00000000000000000000000010101011+4,4,a1*8	lookupswitch	<a0>, <a1>
10000001	lor
01110001	lrem
10101101	lreturn
01111001	lshl
01111011	lshr
00110111+1	lstore	<a0>
0011011111000100+2	lstore	<a0>
00111111	lstore_0
01000000	lstore_1
01000001	lstore_2
01000010	lstore_3
01100101	lsub
01111101	lushr
10000011	lxor
11000010	monitorenter
11000011	monitorexit
11000101+2,1	multianewarray	<a0>, <a1>
1100010111000100+4,2	multianewarray	<a0>, <a1>
10111011+2	new	<a0>
10111100+1	newarray	<a0>
1011110011000100+2	newarray	<a0>
00000000	nop
01010111	pop
01011000	pop2
10110101+2	putfield	<a0>
10110011+2	putstatic	<a0>
10101001+1	ret	<a0>
1010100111000100+2	ret	<a0>
10110001	return
00110101	saload
01010110	sastore
00010001+2	sipush	<a0>
01011111	swap
00000000000000000000000011000100+4,4,4,(a2-a1)*2	tableswitch	<a0>, <a1>, <a2>
