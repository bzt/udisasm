/*
 * Copyright (C) 2017 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Example code on how to use the Universal Disassembler Function Library
 */

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "aarch64.h"
//#include "z80.h"
//#include "java.h"
//#include "x86_64.h"

// required if header generated with -i
uint8_t sys_fault=0;
uint64_t dbg_label, buf_reloc=0;

int main(int argc, char **argv)
{
    FILE *f;
    long int s;
    uint64_t ptr, end;
    char *data, str[1024];

    if(argc<2) {
        fprintf(stderr, "Universal Disassembler by bzt 2017\n\nUsage: %s <raw AArch64 binary>\n", argv[0]);
        exit(1);
    }

    // read input
    f=fopen(argv[1], "r");
    if(f){
        fseek(f, 0L, SEEK_END);
        s=ftell(f);
        fseek(f, 0L, SEEK_SET);
        data=(char*)malloc(s+1);
        if(data==NULL) {
            fprintf(stderr, "Unable to allocate %ld memory\n", s+1);
            exit(2);
        }
        fread(data, s, 1, f);
        fclose(f);
    } else {
        fprintf(stderr, "File not found %s\n", argv[1]);
        exit(1);
    }
    ptr = buf_reloc = (uint64_t)data;
    end = ptr + s;

    // disassemble instructions
    while (ptr<end && ptr!=0) {
        // print out address and instruction in hex
#ifdef disasm_integration
        // if header generated with "-i", addresses will be relative to buffer
        printf("%08x: %08x ", (int)(ptr - buf_reloc), *((uint32_t *)ptr));
#else
        printf("%08x: %08x ", (int)ptr, *((uint32_t *)ptr));
#endif
        // now do the thing, call the library
#ifdef disasm_snprintf
        // if header generated with "-n", there's an additional buffer size argument
        ptr = disasm(ptr, (char*)&str, sizeof(str));
        // print the disassembled instruction buffer
        if (ptr!=0)
            printf("%s\n", str);
        else
            printf("BUFFER TOO SMALL\n");
#else
        // print the disassembled instruction buffer
        ptr = disasm(ptr, (char*)&str);
        printf("%s\n", str);
#endif
    }
}
