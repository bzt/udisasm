/* Universal Disassembler Function Library
 * https://gitlab.com/bztsrc/udisasm
 *
 *            ----- GENERATED FILE, DO NOT EDIT! -----
 *
 * Copyright (C) 2017 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Disassembler source generated from z80.txt
 */

#ifdef __cplusplus
extern "C" {
#endif

#define disasm_arch "z80"
enum { disasm_arg_NONE,disasm_arg_ofs,disasm_arg_ofe, disasm_arg_r, disasm_arg_g, disasm_arg_n, disasm_arg_hlidx, disasm_arg_ixd, disasm_arg_iyd, disasm_arg_A, disasm_arg_bcidx, disasm_arg_deidx, disasm_arg_nidx, disasm_arg_I, disasm_arg_R, disasm_arg_r16, disasm_arg_sp, disasm_arg_de, disasm_arg_hl, disasm_arg_af, disasm_arg_afap, disasm_arg_spidx, disasm_arg_ix, disasm_arg_iy, disasm_arg_nd, disasm_arg_labeln, disasm_arg_c, disasm_arg_labeli, disasm_arg_ixidx, disasm_arg_iyidx, disasm_arg_n8, disasm_arg_Cidx };

/*** private functions ***/
char *disasm_str(char*s,int n) {if(!s)return "?";while(n){s++;if(!*s){s++;n--;}}return *s?s:"?";}

/*** public API ***/
uint64_t disasm(uint64_t addr, char *str)
{
    uint16_t op=0, om=0, n=0;
    uint8_t r=0, g=0, d=0, c=0, i=0;
    uint8_t ic8, ic8_3, ic8_4;
    uint16_t ic16, ic16_11, ic16_12, ic16_8, ic16_10, ic16_3;
    uint32_t ic24, ic24_8, ic24_16;
    uint32_t ic32, ic32_27, ic32_16;
    char *names=NULL,*olds=str;
    char *regs="b\0c\0d\0e\0h\0l\0?\0a\0";
    char *reg16="bc\0de\0hl\0sp\0ix\0iy\0";
    char *conds="nz\0z\0nc\0c\0po\0pe\0p\0m\0";
    uint8_t args[3]={0,0,0};

    ic8=*((uint8_t*)addr);
    ic16=*((uint16_t*)addr);
    ic24=*((uint32_t*)addr);
    ic32=*((uint32_t*)addr);
    ic16_11=ic16>>11; ic16_12=ic16>>12; ic8_3=ic8>>3; ic8_4=ic8>>4; ic32_27=ic32>>27; ic32_16=ic32>>16; ic16_8=ic16>>8; ic16_10=ic16>>10; ic16_3=ic16>>3; ic24_8=ic24>>8; ic24_16=ic24>>16;

    /* handle multiple NOPs at once */
    if(ic8==0x0) {
        while(*((uint8_t*)addr)==ic8) { op++; addr+=1; }
        if(str!=NULL) str+=sprintf(str,"  %d x nop",op);
        *str=0;
        return addr;
    }

    /* decode instruction */
    if(ic8==0x2) {
        names="ld\0";
        args[0]=disasm_arg_bcidx; args[1]=disasm_arg_A; 
        addr+=1;
    } else
    if(ic8==0x8) {
        names="ex\0";
        args[0]=disasm_arg_af; args[1]=disasm_arg_afap; 
        addr+=1;
    } else
    if(ic8==0xa) {
        names="ld\0";
        args[0]=disasm_arg_A; args[1]=disasm_arg_bcidx; 
        addr+=1;
    } else
    if(ic8==0x12) {
        names="ld\0";
        args[0]=disasm_arg_deidx; args[1]=disasm_arg_A; 
        addr+=1;
    } else
    if(ic8==0x1a) {
        names="ld\0";
        args[0]=disasm_arg_A; args[1]=disasm_arg_deidx; 
        addr+=1;
    } else
    if((ic16&0xf7ff)==0x23dd) {
        names="inc\0dec\0";
        op=((ic16_11)&0x1); 
        args[0]=disasm_arg_ix; 
        addr+=2;
    } else
    if((ic16&0xf7ff)==0x23fd) {
        names="inc\0dec\0";
        op=((ic16_11)&0x1); 
        args[0]=disasm_arg_iy; 
        addr+=2;
    } else
    if((ic8&0xfe)==0x34) {
        names="inc\0dec\0";
        op=((ic8)&0x1); 
        args[0]=disasm_arg_hlidx; 
        addr+=1;
    } else
    if((ic16&0xcfff)==0x9dd) {
        names="add\0";
        d=((ic16_12)&0x3); 
        args[0]=disasm_arg_ix; args[1]=disasm_arg_r16; 
        addr+=2;
    } else
    if((ic16&0xcfff)==0x9fd) {
        names="add\0";
        d=((ic16_12)&0x3); 
        args[0]=disasm_arg_iy; args[1]=disasm_arg_r16; 
        addr+=2;
    } else
    if((ic8&0xc7)==0x1) {
        names="sub\0add\0";
        op=((ic8_3)&0x1); d=((ic8_4)&0x3); 
        args[0]=disasm_arg_hl; args[1]=disasm_arg_r16; 
        addr+=1;
    } else
    if((ic8&0xc7)==0x3) {
        names="inc\0dec\0";
        op=((ic8_3)&0x1); d=((ic8_4)&0x3); 
        args[0]=disasm_arg_r16; 
        addr+=1;
    } else
    if((ic8&0xc6)==0x4) {
        names="inc\0dec\0";
        op=((ic8)&0x1); r=((ic8_3)&0x7); 
        args[0]=disasm_arg_r; 
        addr+=1;
    } else
    if((ic16&0xc7ff)==0x6cb) {
        names="rlc\0?\0rl\0rr\0sla\0sra\0srl\0";
        op=((ic16_11)&0x7); 
        args[0]=disasm_arg_hlidx; 
        addr+=2;
    } else
    if((ic32&0xc700ffff)==0x600cbdd) {
        names="rlc\0?\0rl\0rr\0sla\0sra\0srl\0";
        op=((ic32_27)&0x7); d=((ic32_16)&0xff); 
        args[0]=disasm_arg_ixd; 
        addr+=4;
    } else
    if((ic32&0xc700ffff)==0x600cbfd) {
        names="rlc\0?\0rl\0rr\0sla\0sra\0srl\0";
        op=((ic32_27)&0x7); d=((ic32_16)&0xff); 
        args[0]=disasm_arg_iyd; 
        addr+=4;
    } else
    if((ic8&0xc7)==0x7) {
        names="rlca\0rrca\0rla\0rra\0daa\0cpl\0scf\0ccf\0";
        op=((ic8_3)&0x7); 
        addr+=1;
    } else
    if((ic16&0xc0ff)==0xcb) {
        names="rlc\0?\0rl\0rr\0sla\0sra\0srl\0";
        op=((ic16_11)&0x7); r=((ic16_8)&0x7); 
        args[0]=disasm_arg_r; 
        addr+=2;
    } else
    if(ic16==0x47ed) {
        names="ld\0";
        args[0]=disasm_arg_I; args[1]=disasm_arg_A; 
        addr+=2;
    } else
    if(ic16==0x4fed) {
        names="ld\0";
        args[0]=disasm_arg_R; args[1]=disasm_arg_A; 
        addr+=2;
    } else
    if((ic16&0xf6ff)==0x44ed) {
        names="neg\0retn\0?\0reti\0";
        op=((ic16_10)&0x2)|((ic16_8)&0x1); 
        addr+=2;
    } else
    if(ic16==0x57ed) {
        names="ld\0";
        args[0]=disasm_arg_A; args[1]=disasm_arg_I; 
        addr+=2;
    } else
    if(ic16==0x5fed) {
        names="ld\0";
        args[0]=disasm_arg_A; args[1]=disasm_arg_R; 
        addr+=2;
    } else
    if((ic16&0xe7ff)==0x46ed) {
        names="im\0";
        n=((ic16_10)&0x2)|((ic16_12)&0x1); 
        args[0]=disasm_arg_n; 
        addr+=2;
    } else
    if((ic16&0xf7ff)==0x67ed) {
        names="rrd\0rld\0";
        op=((ic16_11)&0x1); 
        addr+=2;
    } else
    if(ic8==0x76) {
        names="halt\0";
        addr+=1;
    } else
    if((ic8&0xf8)==0x70) {
        names="ld\0";
        r=((ic8)&0x7); 
        args[0]=disasm_arg_hlidx; args[1]=disasm_arg_r; 
        addr+=1;
    } else
    if((ic8&0xcf)==0x42) {
        names="sbc\0";
        d=((ic8_4)&0x3); 
        args[0]=disasm_arg_hl; args[1]=disasm_arg_r16; 
        addr+=1;
    } else
    if((ic16&0xcfff)==0x4aed) {
        names="adc\0";
        d=((ic16_12)&0x3); 
        args[0]=disasm_arg_hl; args[1]=disasm_arg_r16; 
        addr+=2;
    } else
    if((ic16&0xc7ff)==0x40ed) {
        names="in\0";
        r=((ic16_11)&0x7); 
        args[0]=disasm_arg_r; args[1]=disasm_arg_Cidx; 
        addr+=2;
    } else
    if((ic16&0xc7ff)==0x41ed) {
        names="out\0";
        r=((ic16_11)&0x7); 
        args[0]=disasm_arg_Cidx; args[1]=disasm_arg_r; 
        addr+=2;
    } else
    if((ic8&0xc7)==0x46) {
        names="ld\0";
        r=((ic8_3)&0x7); 
        args[0]=disasm_arg_r; args[1]=disasm_arg_hlidx; 
        addr+=1;
    } else
    if((ic8&0xc0)==0x40) {
        names="ld\0";
        r=((ic8_3)&0x7); g=((ic8)&0x7); 
        args[0]=disasm_arg_r; args[1]=disasm_arg_g; 
        addr+=1;
    } else
    if(ic8==0xb0) {
        names="ldir\0";
        addr+=1;
    } else
    if((ic16&0xe4ff)==0xa0ed) {
        names="ldi\0cpi\0ini\0outi\0ldd\0cpd\0ind\0outd\0?\0cpir\0inir\0otir\0lddr\0cpdr\0indr\0otdr\0";
        op=((ic16>>9)&0xc)|((ic16_8)&0x3); 
        addr+=2;
    } else
    if((ic8&0xc7)==0x86) {
        names="add\0adc\0sub\0sbc\0and\0xor\0or\0cp\0";
        op=((ic8_3)&0x7); 
        args[0]=disasm_arg_A; args[1]=disasm_arg_hlidx; 
        addr+=1;
    } else
    if((ic8&0xc0)==0x80) {
        names="add\0adc\0sub\0sbc\0and\0xor\0or\0cp\0";
        op=((ic8_3)&0x7); r=((ic8)&0x7); 
        args[0]=disasm_arg_A; args[1]=disasm_arg_r; 
        addr+=1;
    } else
    if(ic8==0xc9) {
        names="ret\0";
        addr+=1;
    } else
    if(ic8==0xd8) {
        names="exx\0";
        addr+=1;
    } else
    if(ic8==0xe3) {
        names="ex\0";
        d=2;
        args[0]=disasm_arg_spidx; args[1]=disasm_arg_r16; 
        addr+=1;
    } else
    if(ic16==0xe3dd) {
        names="ex\0";
        d=4;
        args[0]=disasm_arg_spidx; args[1]=disasm_arg_r16; 
        addr+=2;
    } else
    if(ic16==0xe3fd) {
        names="ex\0";
        d=5;
        args[0]=disasm_arg_spidx; args[1]=disasm_arg_r16; 
        addr+=2;
    } else
    if((ic16&0xfbff)==0xe1dd) {
        names="pop\0push\0";
        op=((ic16_10)&0x1); 
        d=4;
        args[0]=disasm_arg_r16; 
        addr+=2;
    } else
    if((ic16&0xfbff)==0xe1fd) {
        names="pop\0push\0";
        op=((ic16_10)&0x1); 
        d=5;
        args[0]=disasm_arg_r16; 
        addr+=2;
    } else
    if(ic8==0xe9) {
        names="jp\0";
        args[0]=disasm_arg_hlidx; 
        addr+=1;
    } else
    if(ic16==0xe9dd) {
        names="jp\0";
        args[0]=disasm_arg_ixidx; 
        addr+=2;
    } else
    if(ic16==0xe9fd) {
        names="jp\0";
        args[0]=disasm_arg_iyidx; 
        addr+=2;
    } else
    if(ic8==0xeb) {
        names="ex\0";
        args[0]=disasm_arg_de; args[1]=disasm_arg_hl; 
        addr+=1;
    } else
    if(ic8==0xf9) {
        names="ld\0";
        d=2;
        args[0]=disasm_arg_sp; args[1]=disasm_arg_r16; 
        addr+=1;
    } else
    if(ic16==0xf9dd) {
        names="ld\0";
        d=4;
        args[0]=disasm_arg_sp; args[1]=disasm_arg_r16; 
        addr+=2;
    } else
    if(ic16==0xf9fd) {
        names="ld\0";
        d=5;
        args[0]=disasm_arg_sp; args[1]=disasm_arg_r16; 
        addr+=2;
    } else
    if((ic8&0xf7)==0xf3) {
        names="di\0ei\0";
        op=((ic8_3)&0x1); 
        addr+=1;
    } else
    if((ic8&0xcb)==0xc1) {
        names="pop\0push\0";
        op=((ic8>>2)&0x1); d=((ic8_4)&0x3); 
        args[0]=disasm_arg_r16; 
        addr+=1;
    } else
    if((ic8&0xc7)==0xc0) {
        names="ret\0";
        c=((ic8_3)&0x7); 
        args[0]=disasm_arg_c; 
        addr+=1;
    } else
    if((ic8&0xc7)==0xc7) {
        names="rst\0";
        n=((ic8_3)&0x7); 
        args[0]=disasm_arg_n8; 
        addr+=1;
    } else
    if(ic16==0x6cb) {
        names="?\0bit\0res\0set\0";
        op=((ic16>>14)&0x3); n=((ic16_11)&0x7); 
        args[0]=disasm_arg_nd; args[1]=disasm_arg_hlidx; 
        addr+=2;
    } else
    if((ic32&0x700ffff)==0x600cbdd) {
        names="?\0bit\0res\0set\0";
        op=((ic32>>30)&0x3); n=((ic32_27)&0x7); d=((ic32_16)&0xff); 
        args[0]=disasm_arg_nd; args[1]=disasm_arg_ixd; 
        addr+=4;
    } else
    if((ic32&0x700ffff)==0x600cbfd) {
        names="?\0bit\0res\0set\0";
        op=((ic32>>30)&0x3); n=((ic32_27)&0x7); d=((ic32_16)&0xff); 
        args[0]=disasm_arg_nd; args[1]=disasm_arg_iyd; 
        addr+=4;
    } else
    if((ic16&0xf7)==0x10) {
        names="djnz\0jr\0";
        op=((ic16_3)&0x1); i=((ic16>>15)&1?(0xffffffff<<8):0)|((ic16_8)&0xff); 
        args[0]=disasm_arg_labeli; 
        addr+=2;
    } else
    if((ic24&0xfeff)==0x34dd) {
        names="inc\0dec\0";
        op=((ic24_8)&0x1); d=((ic24_16)&0xff); 
        args[0]=disasm_arg_ixd; 
        addr+=3;
    } else
    if((ic24&0xfeff)==0x34fd) {
        names="inc\0dec\0";
        op=((ic24_8)&0x1); d=((ic24_16)&0xff); 
        args[0]=disasm_arg_iyd; 
        addr+=3;
    } else
    if(ic16==0x36) {
        names="ld\0";
        n=((ic16_8)&0xff); 
        args[0]=disasm_arg_hlidx; args[1]=disasm_arg_n; 
        addr+=2;
    } else
    if((ic16&0xe7)==0x20) {
        names="jr\0";
        i=((ic16>>15)&1?(0xffffffff<<8):0)|((ic16_8)&0xff); c=((ic16_3)&0x3); 
        args[0]=disasm_arg_c; args[1]=disasm_arg_labeli; 
        addr+=2;
    } else
    if((ic16&0xc7)==0x6) {
        names="ld\0";
        n=((ic16_8)&0xff); r=((ic16_3)&0x7); 
        args[0]=disasm_arg_r; args[1]=disasm_arg_n; 
        addr+=2;
    } else
    if((ic24&0xf8ff)==0x70dd) {
        names="ld\0";
        d=((ic24_16)&0xff); r=((ic24_8)&0x7); 
        args[0]=disasm_arg_ixd; args[1]=disasm_arg_r; 
        addr+=3;
    } else
    if((ic24&0xf8ff)==0x70fd) {
        names="ld\0";
        d=((ic24_16)&0xff); r=((ic24_8)&0x7); 
        args[0]=disasm_arg_iyd; args[1]=disasm_arg_r; 
        addr+=3;
    } else
    if((ic24&0xc7ff)==0x46dd) {
        names="ld\0";
        d=((ic24_16)&0xff); r=((ic24>>11)&0x7); 
        args[0]=disasm_arg_r; args[1]=disasm_arg_ixd; 
        addr+=3;
    } else
    if((ic24&0xc7ff)==0x46fd) {
        names="ld\0";
        d=((ic24_16)&0xff); r=((ic24>>11)&0x7); 
        args[0]=disasm_arg_r; args[1]=disasm_arg_iyd; 
        addr+=3;
    } else
    if((ic24&0xc6ff)==0x86dd) {
        names="add\0?\0adc\0?\0sub\0sbc\0?\0?\0and\0?\0xor\0?\0or\0?\0cp\0";
        op=((ic24>>10)&0xe)|((ic24_8)&0x1); d=((ic24_16)&0xff); 
        args[0]=disasm_arg_A; args[1]=disasm_arg_ixd; 
        addr+=3;
    } else
    if((ic24&0xc6ff)==0x86fd) {
        names="add\0?\0adc\0?\0sub\0sbc\0?\0?\0and\0?\0xor\0?\0or\0?\0cp\0";
        op=((ic24>>10)&0xe)|((ic24_8)&0x1); d=((ic24_16)&0xff); 
        args[0]=disasm_arg_A; args[1]=disasm_arg_iyd; 
        addr+=3;
    } else
    if(ic16==0xcb) {
        names="?\0bit\0res\0set\0";
        op=((ic16>>14)&0x3); n=((ic16_11)&0x7); r=((ic16_8)&0x7); 
        args[0]=disasm_arg_nd; args[1]=disasm_arg_r; 
        addr+=2;
    } else
    if(ic16==0xd3) {
        names="out\0";
        n=((ic16_8)&0xff); 
        args[0]=disasm_arg_nidx; args[1]=disasm_arg_A; 
        addr+=2;
    } else
    if(ic16==0xdb) {
        names="in\0";
        n=((ic16_8)&0xff); 
        args[0]=disasm_arg_A; args[1]=disasm_arg_nidx; 
        addr+=2;
    } else
    if((ic16&0xc7)==0xc6) {
        names="add\0adc\0sub\0sbc\0and\0xor\0or\0cp\0";
        op=((ic16_3)&0x7); n=((ic16_8)&0xff); 
        args[0]=disasm_arg_A; args[1]=disasm_arg_n; 
        addr+=2;
    } else
    if(ic32==0x21dd) {
        names="ld\0";
        n=((ic32_16)&0xffff); 
        d=4;
        args[0]=disasm_arg_r16; args[1]=disasm_arg_n; 
        addr+=4;
    } else
    if(ic32==0x21fd) {
        names="ld\0";
        n=((ic32_16)&0xffff); 
        d=5;
        args[0]=disasm_arg_r16; args[1]=disasm_arg_n; 
        addr+=4;
    } else
    if((ic24&0xff)==0x22) {
        names="ld\0";
        n=((ic24_8)&0xffff); 
        d=2;
        args[0]=disasm_arg_nidx; args[1]=disasm_arg_r16; 
        addr+=3;
    } else
    if(ic32==0x22dd) {
        names="ld\0";
        n=((ic32_16)&0xffff); 
        d=4;
        args[0]=disasm_arg_nidx; args[1]=disasm_arg_r16; 
        addr+=4;
    } else
    if(ic32==0x22fd) {
        names="ld\0";
        n=((ic32_16)&0xffff); 
        d=5;
        args[0]=disasm_arg_nidx; args[1]=disasm_arg_r16; 
        addr+=4;
    } else
    if(ic32==0x29dd) {
        names="ld\0";
        n=((ic32_16)&0xffff); 
        d=4;
        args[0]=disasm_arg_r16; args[1]=disasm_arg_nidx; 
        addr+=4;
    } else
    if(ic32==0x29fd) {
        names="ld\0";
        n=((ic32_16)&0xffff); 
        d=5;
        args[0]=disasm_arg_r16; args[1]=disasm_arg_nidx; 
        addr+=4;
    } else
    if((ic24&0xff)==0x2a) {
        names="ld\0";
        n=((ic24_8)&0xffff); 
        d=2;
        args[0]=disasm_arg_r16; args[1]=disasm_arg_nidx; 
        addr+=3;
    } else
    if((ic24&0xff)==0x32) {
        names="ld\0";
        n=((ic24_8)&0xffff); 
        args[0]=disasm_arg_nidx; args[1]=disasm_arg_A; 
        addr+=3;
    } else
    if(ic32==0x36dd) {
        names="ld\0";
        n=((ic32>>24)&0xff); d=((ic32_16)&0xff); 
        args[0]=disasm_arg_ixd; args[1]=disasm_arg_n; 
        addr+=4;
    } else
    if(ic32==0x36fd) {
        names="ld\0";
        n=((ic32>>24)&0xff); d=((ic32_16)&0xff); 
        args[0]=disasm_arg_iyd; args[1]=disasm_arg_n; 
        addr+=4;
    } else
    if((ic24&0xff)==0x3a) {
        names="ld\0";
        n=((ic24_8)&0xffff); 
        args[0]=disasm_arg_A; args[1]=disasm_arg_nidx; 
        addr+=3;
    } else
    if((ic24&0xcf)==0x1) {
        names="ld\0";
        n=((ic24_8)&0xffff); d=((ic24>>4)&0x3); 
        args[0]=disasm_arg_r16; args[1]=disasm_arg_n; 
        addr+=3;
    } else
    if((ic24&0xcf)==0x43) {
        names="ld\0";
        n=((ic24_8)&0xffff); d=((ic24>>4)&0x3); 
        args[0]=disasm_arg_nidx; args[1]=disasm_arg_r16; 
        addr+=3;
    } else
    if((ic32&0xcfff)==0x4bed) {
        names="ld\0";
        n=((ic32_16)&0xffff); d=((ic32>>12)&0x3); 
        args[0]=disasm_arg_r16; args[1]=disasm_arg_nidx; 
        addr+=4;
    } else
    if((ic24&0xff)==0xc3) {
        names="jp\0";
        n=((ic24_8)&0xffff); 
        args[0]=disasm_arg_labeln; 
        addr+=3;
    } else
    if((ic24&0xff)==0xcd) {
        names="call\0";
        n=((ic24_8)&0xffff); 
        args[0]=disasm_arg_labeln; 
        addr+=3;
    } else
    if((ic24&0xc7)==0xc2) {
        names="jp\0";
        n=((ic24_8)&0xffff); c=((ic24>>3)&0x7); 
        args[0]=disasm_arg_c; args[1]=disasm_arg_labeln; 
        addr+=3;
    } else
    if((ic24&0xc7)==0xc4) {
        names="call\0";
        n=((ic24_8)&0xffff); c=((ic24>>3)&0x7); 
        args[0]=disasm_arg_c; args[1]=disasm_arg_labeln; 
        addr+=3;
    } else
        names=NULL;

    if(str!=NULL) {
        str+=sprintf(str,disasm_str(names,op),disasm_str(conds,c));
        if(str-olds<8)om=8-(str-olds);else om=1;for(op=0;op<om;op++) *str++=' ';
        for(op=0;op<sizeof(args) && args[op]!=disasm_arg_NONE;op++) {
            if(op) { *str++=','; *str++=' '; }
            switch(args[op]) {
                case disasm_arg_r: str+=sprintf(str,"%s", disasm_str(regs,r)); break;
                case disasm_arg_g: str+=sprintf(str,"%s", "%s'", disasm_str(regs,g)); break;
                case disasm_arg_n: str+=sprintf(str,"%xh", n); break;
                case disasm_arg_hlidx: str+=sprintf(str,"(hl)"); break;
                case disasm_arg_ixd: str+=sprintf(str,"(ix+%d)", d); break;
                case disasm_arg_iyd: str+=sprintf(str,"(iy+%d)", d); break;
                case disasm_arg_A: str+=sprintf(str,"a"); break;
                case disasm_arg_bcidx: str+=sprintf(str,"(bc)"); break;
                case disasm_arg_deidx: str+=sprintf(str,"(de)"); break;
                case disasm_arg_nidx: str+=sprintf(str,"(%xh)", n); break;
                case disasm_arg_I: str+=sprintf(str,"i"); break;
                case disasm_arg_R: str+=sprintf(str,"r"); break;
                case disasm_arg_r16: str+=sprintf(str,"%s", disasm_str(reg16,d)); break;
                case disasm_arg_sp: str+=sprintf(str,"sp"); break;
                case disasm_arg_de: str+=sprintf(str,"de"); break;
                case disasm_arg_hl: str+=sprintf(str,"hl"); break;
                case disasm_arg_af: str+=sprintf(str,"af"); break;
                case disasm_arg_afap: str+=sprintf(str,"af'"); break;
                case disasm_arg_spidx: str+=sprintf(str,"(sp)"); break;
                case disasm_arg_ix: str+=sprintf(str,"ix"); break;
                case disasm_arg_iy: str+=sprintf(str,"iy"); break;
                case disasm_arg_nd: str+=sprintf(str,"%d", n); break;
                case disasm_arg_labeln: str+=sprintf(str,"%xh", n); break;
                case disasm_arg_c: str+=sprintf(str,"%s", disasm_str(conds,c)); break;
                case disasm_arg_labeli: str+=sprintf(str,"%lxh", ((int)addr)+i); break;
                case disasm_arg_ixidx: str+=sprintf(str,"(ix)"); break;
                case disasm_arg_iyidx: str+=sprintf(str,"(iy)"); break;
                case disasm_arg_n8: str+=sprintf(str,"%xh", n<<3); break;
                case disasm_arg_Cidx: str+=sprintf(str,"(c)"); break;
                default: break;
            }
            if(*(str-2)==',')str-=2;
        }
        *str=0;
    }
    return addr;
}

#ifdef __cplusplus
}
#endif

